const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')
const cors = require('cors')
const user_routes = require('./routes/users')
const product_routes = require('./routes/products')
const order_routes = require('./routes/orders')
const category_routes = require('./routes/categories')

dotenv.config(); //setup the ENV

let account = process.env.CREDENTIALS
const port = process.env.PORT 

// Server Setup
const app = express()
app.use(express.json())
app.use(express.static(__dirname))
app.use(cors())

// Routing
app.use('/api/users', user_routes)
app.use('/api/products', product_routes)
app.use('/api/orders', order_routes)
app.use('/api/categories', category_routes)

// Database Connection 
mongoose.connect(account);
const connectStatus = mongoose.connection; 
connectStatus.once('open', () => console.log(`Database Connected`));

// Run server
app.get('/', (request, response) => {
    response.send('Welcome to Bicycle Cartel API! Created by Earl Diaz.')
})

app.listen(port, () => {
    console.log(`API is running on port ${port}`)
})