const mongoose = require('mongoose')

let user_schema = new mongoose.Schema({
    first_name: {
        type: String,
        required: [true, 'First Name is required']
    },
    last_name: {
        type: String,
        required: [true, 'Last Name is required']
    },
    mobile_number: {
        type: String,
        required: [true, 'Last Name is required']
    },
    email: {
        type: String,
        required: [true, 'Email is required']
    },
    password: {
        type: String,
        required: [true, 'Password is required']
    },
    is_admin: {
        type: Boolean,
        default: false
    }
})

module.exports = mongoose.model('User', user_schema)