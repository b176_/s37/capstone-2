const mongoose = require('mongoose')

let product_schema = new mongoose.Schema({
    image: {
        type: String,
        required: [true, 'Product image is required']
    },
    name: {
        type: String,
        required: [true, 'Product name is required']
    },
    description: {
        type: String,
        required: [true, 'Product description is required']
    },
    price: {
    type: Number,
        required: [true, 'Product price is required']
    },
    category_id: {
        type: String,
        required: [true, 'Category ID is required']
    },
    is_active: {
        type: Boolean,
        default: true
    },
    created_on: {
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model('Product', product_schema)