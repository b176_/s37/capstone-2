const mongoose = require('mongoose')

let category_schema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Category Name is required']
    }
})

module.exports = mongoose.model('Category', category_schema)