const mongoose = require('mongoose')

let order_schema = new mongoose.Schema({
    total_amount: {
        type: Number,
        required: [true, 'Order total amount is required']
    },
    purchased_on: {
        type: Date,
        default: new Date()
    },
    user_id: {
        type: String,
        required: [true, 'Order user ID is required']
    },
    order_products: [
        {
            product_id: {
                type: String,
                require: [true, 'Product ID for order is required.']
            },
            name: {
                type: String,
                default: ''
            },
            description: {
                type: String,
                default: ''
            },
            price: {
                type: Number,
                default: 0
            },
            quantity: {
                type: Number,
                default: 1
            }
        }
    ]
})

module.exports = mongoose.model('Order', order_schema)