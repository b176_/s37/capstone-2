const express = require('express')
const controller = require('../controllers/products')
const auth = require('../auth')
const uploadImage = require('../middlewares/uploadImage')

const router = express.Router()
const { verify, verifyAdmin } = auth 

router.post('/create', verify, verifyAdmin, uploadImage.single('image'), (request, response) => { 
        controller.createOne(request.body, request.file.location).then(result => {
            response.send(result)
    })
})

router.get('/active', (request, response) => {
    controller.getAllActive().then(result => {
        response.send(result)
    })
})

router.get('/', (request, response) => {
    controller.getAll().then(result => {
        response.send(result)
    })
})

router.get('/:id', (request, response) => {
    controller.getOne(request.params.id).then(result => {
        response.send(result)
    })
})

router.get('/:search_criteria/search-active', (request, response) => {
    controller.searchActive(request.params.search_criteria).then(result => {
        response.send(result)
    })
})

router.get('/:category_id/filter', (request, response) => {
    controller.filterByCategory(request.params.category_id).then(result => {
        response.send(result)
    })
})

router.patch('/:id/update', verify, verifyAdmin, uploadImage.single('image'), (request, response) => {
    controller.updateOne(request.params.id, request.file?.location || null, request.body).then(result => {
        response.send(result)
    })
})

router.patch('/:id/archive', verify, verifyAdmin, (request, response) => {
    controller.archiveOne(request.params.id).then(result => {
        response.send(result)
    })
})

router.patch('/:id/activate', verify, verifyAdmin, (request, response) => {
    controller.activateOne(request.params.id).then(result => {
        response.send(result)
    })
})

router.delete('/:id/delete', verify, verifyAdmin, (request, response) => {
    controller.deleteOne(request.params.id).then(result => {
        response.send(result)
    })
})

module.exports = router
