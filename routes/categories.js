const express = require('express')
const controller = require('../controllers/categories')
const auth = require('../auth')

const router = express.Router()
const { verify, verifyAdmin } = auth 

router.get('/', (request, response) => {
    controller.getAll().then(result => {
        response.send(result)
    })
})

router.get('/:id', (request, response) => {
    controller.getOne(request.params.id).then(result => {
        response.send(result)
    })
})

module.exports = router