const express = require('express')
const controller = require('../controllers/orders')
const auth = require('../auth')

const router = express.Router()
const { verify, verifyAdmin } = auth 

router.post('/create', verify, (request, response) => {
    controller.createOne(request.user.id, request.body).then(result => {
        response.send(result)
    })
})

router.get('/auth', verify, (request, response) => {
    controller.getAllAuth(request.user.id).then(result => {
        response.send(result)
    })
})

router.get('/', verify, verifyAdmin, (request, response) => {
    controller.getAll().then(result => {
        response.send(result)
    })
})

// router.get('/order-products', verify, (request, response) => {
//     const products = request.body.order_products

//     controller.getProducts(products).then(result => {
//         response.send(result)
//     })
// })

module.exports = router