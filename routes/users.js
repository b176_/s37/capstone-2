const express = require('express')
const controller = require('../controllers/users')
const auth = require('../auth')

const router = express.Router()
const { verify, verifyAdmin } = auth

router.post('/register', (request, response) => {
    let new_user = request.body

    controller.register(new_user).then(result => {
        response.send(result)
    })
})

router.post('/login', (request, response) => {
    controller.login(request.body).then(result => {
        response.send(result)
    })
})

router.patch('/:id/set-admin', verify, verifyAdmin, (request, response) => {
    controller.setAdmin(request.params.id).then(result => {
        response.send(result)
    })
})

router.get('/', verify, verifyAdmin, (request, response) => {
    controller.getAllAdmin().then(result => {
        response.send(result)
    })
})

router.get("/details", auth.verify, (req, res) => {
    controller.getUser(req.user.id)
        .then(result => res.send(result))
        .catch(err => res.send(err.message));
});

module.exports = router