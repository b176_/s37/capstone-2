const User = require('../models/User')
const bcrypt = require('bcrypt')
const dotenv = require('dotenv')
const auth = require('../auth')

dotenv.config()

module.exports.register = (user_data) => {
    let new_user = new User({
        first_name: user_data.first_name,
        last_name: user_data.last_name,
        email: user_data.email,
        password: bcrypt.hashSync(user_data.password, parseInt(process.env.SALT)),
        // 1st parameter - password to be hashed
        // 2nd parameter - how many times to be hashed
        mobile_number: user_data.mobile_number
    })

    return new_user.save().then((user, error) => {
        if (user) {
            return 'User registered successfully!'
        } else {
            return error
        }
    })
}

module.exports.login = (user_data) => {
    // check if user exists
    return User.findOne({
        email: user_data.email
    }).then(result => {
        if(!result){
            return false 
        }

        const password_match = bcrypt.compareSync(user_data.password, result.password)

        if(password_match){
            // convert result to an object
            return {accessToken: auth.createAccessToken(result.toObject())}
        } else {
            return false
        }
    })
}

module.exports.setAdmin = (user_id) => {
    return User.findByIdAndUpdate(user_id, {
        is_admin: true
    }).then((updated_user, error) => {
        if(error)
        {
            return error
        }

        return {
            message: 'User has been set to ADMIN!',
            result: updated_user
        }
    }).catch(error => {
        return error.message
    })
}

module.exports.getAllAdmin = () => {
    return User.find({}).then((users, error) => {
        if(error){
            return error
        }

        return users
    })
}

module.exports.getUser = (user_id) => {
    return User.findById(user_id)
        .then(result => {
            // change value of password to empty string
            result.password = '';
            return result;
        })
        .catch(err => err.message);
}