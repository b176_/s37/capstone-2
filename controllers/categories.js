const Category = require('../models/Category')

module.exports.getAll = () => {
    return Category.find({}).then((categories, error) => {
        if(error){
            return error 
        } 

        return categories
    }).catch(error => {
        return error.message
    })
}

module.exports.getOne = (category_id) => {
    return Category.findById(category_id).then((category, error) => {
        if(error){
            return error 
        } 

        return category
    }).catch(error => {
        return error.message
    })
}