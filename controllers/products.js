const Product = require('../models/Product')

module.exports.createOne = (product, image) => {
    let new_product = new Product({
        image: image,
        name: product.name,
        description: product.description,
        price: product.price,
        category_id: product.category_id
    })

    return new_product.save().then((saved_product, error) => {
        if(error) {
            return error
        }

        return { 
            message: 'New product created successfully!',
            result: saved_product
        }
    }).catch(error => {
        return error.message
    })
}

module.exports.getAllActive = () => {
    return Product.find({
        is_active: true
    }).then((products, error) => {
        if(error){
            return error
        }

        return products
    }).catch(error => {
        return error.message
    })
}

module.exports.getAll = () => {
    return Product.find({}).then((products, error) => {
        if(error){
            return error
        }

        return products
    }).catch(error => {
        return error.message
    })
}

module.exports.getOne = (product_id) => {
    return Product.findById(product_id).then((product, error) => {
        if(error){
            return error
        }

        return product
    }).catch(error => {
        return error.message
    })
}

module.exports.searchActive = (search_criteria) => {
    return Product.find({
        is_active: true,
        $or:[
            {
                name: { $regex: search_criteria, $options: 'i' }
            }, 
            {
                description: { $regex: search_criteria, $options: 'i' }
            }
        ]
    }).then((search_results, error) => {
        if(error){
            return error 
        }

        return search_results
    })
}

module.exports.filterByCategory = (category_id) => {
    return Product.find({
        category_id: category_id
    })
    .then((products, error) => {
        if(error){
            return error 
        }

        return products
    })
}

module.exports.updateOne = (product_id, new_image, new_data) => {
    let updated_data = {
        image: new_image || new_data.image, //checks if user uploaded a new image, if not then use the previous image
        name: new_data.name,
        description: new_data.description,
        price: new_data.price,
        category_id: new_data.category_id
    }

    return Product.findByIdAndUpdate(product_id, updated_data).then((updated_product, error) => {
        if(error){
            return {
                error: error.message
            }
        }

        return {
            message: 'Product updated successfully!',
            result: updated_product
        }
    }).catch(error => {
        return {
            error: error.message
        }
    })
}

module.exports.archiveOne = (product_id) => {
    return Product.findByIdAndUpdate(product_id, {
        is_active: false 
    }).then((archived_product, error) => {
        if(error){
            return error
        }

        return {
            message: 'Product has been archived!',
            result: archived_product
        }
    })
}

module.exports.activateOne = (product_id) => {
    return Product.findByIdAndUpdate(product_id, {
        is_active: true 
    }).then((archived_product, error) => {
        if(error){
            return error
        }

        return {
            message: 'Product has been archived!',
            result: archived_product
        }
    })
}

module.exports.deleteOne = (product_id) => {
    return Product.findByIdAndDelete(product_id).then((deleted_product, error) => {
        if(error) {
            return error
        }

        return {
            message: 'Product has been deleted successfully!'
        }
    })
}