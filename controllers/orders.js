const Order = require('../models/Order')
const Product = require('../models/Product')

module.exports.createOne = (user_id, products) => {
    let sum_of_products = 0
    let order_contents = products.order_products

    // get sum of each product price
    order_contents.forEach(product => {
        let subtotal = product.quantity * product.price
        sum_of_products += subtotal
    });

    let new_order = new Order()

    new_order.total_amount = sum_of_products
    new_order.user_id = user_id

    // push each product object into the order_products array
    order_contents.forEach(product => {
        new_order.order_products.push(product)
    })

    return new_order.save().then((saved_order, error) => {
        if(error){
            return error
        }

        return {
            message: 'Order successfully created!',
            result: saved_order
        }
    }).catch(error => {
        return error.message
    })
}

module.exports.getAllAuth = (id) => {
    return Order.find({
        user_id: id
    }).then((orders, error) => {
        if(error)
        {
            return error
        }

        return orders
    }).catch(error => {
        return error.message
    })
}

// module.exports.getProducts = (products) => {
//     let product_ids = []

//     products.forEach(product => {
//         product_ids.push(product.product_id)
//     })

//     return Product.find({ '_id': { $in: product_ids } }).then((order_products, error) =>{
//         if(error){
//             return error 
//         }

//         return order_products
//     })
// }

module.exports.getAll = () => {
    return Order.find({}).then((orders, error) => {
        if (error) {
            return error
        }

        return orders
    }).catch(error => {
        return error.message
    })
}